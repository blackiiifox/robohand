#include "file_system.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h> 

//==============================================================================
//============================File system work==================================
//==============================================================================
HAL_StatusTypeDef       flash_ok = HAL_ERROR;
void SaveAllCommands(ListCommands* listCommands)
{
  
  ClearCommandFlash();
  
  // Save commands
  flash_ok = HAL_ERROR;
  while(flash_ok != HAL_OK){
    flash_ok = HAL_FLASH_Unlock();
  }
  
  StructCommand* command = NULL;
  uint32_t count_command =  ShowCountCommand(listCommands);
  uint32_t AddressPage = StartPage;
  uint32_t num_page = 0;
  uint32_t offsetIndexCommand = 0;
  
  for (uint32_t indexCommand = 0; indexCommand < count_command; indexCommand++)
  {
    if (indexCommand%4 == 0 && indexCommand!=0)
    {
      num_page++;
      AddressPage = StartPage + (1024 * num_page);
      offsetIndexCommand = 0;
    }
    uint32_t address = AddressPage + (200 * offsetIndexCommand);
    
    command =  GetCommandById(listCommands, indexCommand);
    if (command == NULL)
    {
      continue;
    }
    offsetIndexCommand++;

    // Save name
    uint32_t base_address_name = address;
    for (int i = 0; i < 5; i++)
    {
      // Convert name
      uint32_t name;
      name = command->name_command[i*4];
      name |= command->name_command[i*4 + 1] << 8;
      name |= command->name_command[i*4 + 2] << 16;
      name |= command->name_command[i*4 + 3] << 24;
      // Save name
      uint32_t address_byte = base_address_name + (4*i);
      
      flash_ok = HAL_ERROR;
      while(flash_ok != HAL_OK){
        flash_ok = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address_byte, name);
      }	
    }
    
    // Save date
    uint32_t base_address_date = address + 0x14;
    for (int i = 0; i < 3; i++)
    {
      // Convert date
      uint32_t date;
      date = command->info.date[i*4];
      date |= command->info.date[i*4 + 1] << 8;
      date |= command->info.date[i*4 + 2] << 16;
      date |= command->info.date[i*4 + 3] << 24;
      
      // Save date
      uint32_t address_byte = base_address_date + (4*i);
      flash_ok = HAL_ERROR;
      while(flash_ok != HAL_OK){
        flash_ok = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address_byte, date);
      }	
    }
    
    // Save other info
    uint32_t address_other = address + 0x20;
    uint32_t info = 0x0;
    info = command->info.count_actions;
    info |=   command->info.num_act_rep << 8;
    info |=   command->info.iterable_actions << 16;
    info |=   command->info.combined << 24;
    flash_ok = HAL_ERROR;
    while(flash_ok != HAL_OK){
        flash_ok = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address_other, info);
      }	
    
    // Save action
    uint32_t base_address_action = address + 0x24;
    for(int i = 0; i < command->info.count_actions; i++)
    {
      // Convert action
      StructAction *action = (StructAction*)command->addr_actions[i];
      uint32_t action_low = 0, action_high = 0; 
      
      action_low = action->pointer_finger;
      action_low |= action->middle_finger << 8;
      action_low |= action->ring_finder << 16;
      action_low |= action->little_finger << 24;
      
      action_high = action->thumb_finger;
      action_high |= action->state_pos_brush << 8;
      action_high |= action->state_pos_thumb << 16;
      action_high |= action->del_action << 24;
      
      // Save action
      uint32_t address_byte_low = base_address_action + (i*8);
      uint32_t address_byte_high = base_address_action + (i*8) + 4;
      flash_ok = HAL_ERROR;
      while(flash_ok != HAL_OK){
        flash_ok = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address_byte_low, action_low);
      }	
      flash_ok = HAL_ERROR;
      while(flash_ok != HAL_OK){
        flash_ok = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address_byte_high, action_high);
      }	
    }
    
  }
  
  flash_ok = HAL_ERROR;
  while(flash_ok != HAL_OK){
    flash_ok = HAL_FLASH_Lock();
  }
}

char* GetNameById(int id)
{
  int count_commands = 0;
  uint32_t AddressPage = StartPage;
  uint32_t num_page = 0;
  uint32_t offsetIndexCommand = 0;
  char *nameCommandInSystem = malloc(sizeof(char)*20);
  for (int indexCommand = 0; indexCommand < SizePage * 5; indexCommand++)
  {
    
    if (indexCommand%4 == 0 && indexCommand!=0)
    {
      num_page++;
      if(SizePage == num_page)
        break;
      AddressPage = StartPage + (1024 * num_page);
      offsetIndexCommand = 0;
    }
    uint32_t address_command = AddressPage + (200 * offsetIndexCommand);
    
    offsetIndexCommand++;
    
    LoadString(nameCommandInSystem, (char*)address_command, 20);
    
    if(!(nameCommandInSystem[0] == '�' && nameCommandInSystem[1] == '�'))
      count_commands++;
    
    if(count_commands == id + 1)
      return nameCommandInSystem;
  }
  free(nameCommandInSystem);
  nameCommandInSystem = NULL;
  return nameCommandInSystem;
}

int ShowCountCommands()
{
  int count_commands = 0;
  uint32_t AddressPage = StartPage;
  uint32_t num_page = 0;
  uint32_t offsetIndexCommand = 0;
  char *nameCommandInSystem = malloc(sizeof(char)*20);
  for (int indexCommand = 0; indexCommand < SizePage * 5; indexCommand++)
  {
    
    if (indexCommand%4 == 0 && indexCommand!=0)
    {
      num_page++;
      if(SizePage == num_page)
        break;
      AddressPage = StartPage + (1024 * num_page);
      offsetIndexCommand = 0;
    }
    uint32_t address_command = AddressPage + (200 * offsetIndexCommand);
    
    offsetIndexCommand++;
    
    LoadString(nameCommandInSystem, (char*)address_command, 20);
    
    if(!(nameCommandInSystem[0] == '�' && nameCommandInSystem[1] == '�'))
      count_commands++;
  }
  free(nameCommandInSystem);
  
  return count_commands;
}

StructCommand* LoadCommand(char* nameCommand)
{
  StructCommand* command = NULL;
  char *nameCommandInSystem = malloc(sizeof(char)*20);
  // char nameCommandInSystem[20];
  
  uint32_t AddressPage = StartPage;
  uint32_t num_page = 0;
  uint32_t offsetIndexCommand = 0;
  
  for (int indexCommand = 0; indexCommand < SizePage * 5; indexCommand++)
  {
    
    if (indexCommand%4 == 0 && indexCommand!=0)
    {
      num_page++;
      if(SizePage == num_page)
      {
        break;
      }
      AddressPage = StartPage + (1024 * num_page);
      offsetIndexCommand = 0;
    }
    uint32_t address_command = AddressPage + (200 * offsetIndexCommand);
    
    offsetIndexCommand++;
    
    LoadString(nameCommandInSystem, (char*)address_command, 20);
    
    if(strcmp (nameCommandInSystem, nameCommand) == 0)
    {
      command = CreateCommand(nameCommandInSystem);
      
      // info load
      char *dateCommandInSystem = malloc(sizeof(char)*12);
      LoadString(dateCommandInSystem, (char*)(address_command + 0x14), 12);
      command->info.date = dateCommandInSystem;
      
      uint32_t info = *((uint32_t*)(address_command + 0x20));
      command->info.count_actions = info&0x000000FF;
      command->info.num_act_rep = (info&0x0000FF00) >> 8;
      command->info.iterable_actions =(info&0x00FF0000) >> 16;
      command->info.combined = (info&0xFF000000) >> 24;
      
      uint32_t address_action = address_command + 0x24;
      for (int i = 0; i < command->info.count_actions; i++)
      {
        StructAction *action = CreateAction();
        uint32_t action_low = *(uint32_t*)address_action;
        uint32_t action_high = *(uint32_t*)((uint32_t*)address_action + 1);
        
        action->pointer_finger = action_low&0x000000FF;
        action->middle_finger = (action_low&0x0000FF00) >> 8;
        action->ring_finder = (action_low&0x00FF0000) >> 16;
        action->little_finger = (action_low&0xFF000000) >> 24;
        
        action->thumb_finger = action_high&0x000000FF;
        action->state_pos_brush = (action_high&0x0000FF00) >> 8;
        action->state_pos_thumb = (action_high&0x00FF0000) >> 16;
        action->del_action = (action_high&0xFF000000) >> 24;
        address_action = address_action + 8;
        command->addr_actions[i] = (uint32_t)action;
      }
      
      return command;
    }
  }
  // if not finde delete char array and return null
  free(nameCommandInSystem);
  return command;
}

void LoadString(char* name, char* address, uint32_t size)
{
  for (int i = 0; i < size; i++)
    {
      name[i] = *(address + i);
    }
}



void ClearCommandFlash()
{
  // Clear page in flash
  uint32_t PAGEError = 0;
  flash_ok = HAL_ERROR;
  while(flash_ok != HAL_OK){
    flash_ok = HAL_FLASH_Unlock();
  }
  
  FLASH_EraseInitTypeDef pEraseInit;
  pEraseInit.PageAddress = StartPage;
  pEraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
  pEraseInit.NbPages =  SizePage;
  HAL_FLASHEx_Erase(&pEraseInit, &PAGEError);
  
  flash_ok = HAL_ERROR;
  while(flash_ok != HAL_OK){
    flash_ok = HAL_FLASH_Lock();
  }
}