#ifndef file_system
#define file_system
#include "stm32f1xx_hal.h"
#include "commands.h"

// file_system.h usage example 

// LOAD COMMAND
//  char *name1 = malloc(sizeof(char)*20);
//  name1[0] = '1';
//  name1[1] = '4';
//  for (int i = 2; i < 20; i++)
//  {
//    name1[i] = 0;
//  }
//  name1[18] = '3';
//  name1[19] = '4';
//  StructCommand* command = LoadCommand(name1);


// SAVE COMMAND
//  ListCommands* listCommands = CreateListCommands();
//  
//  char *name1 = malloc(sizeof(char)*20);
//  name1[0] = '3';
//  name1[1] = '4';
//  for (int i = 2; i < 20; i++)
//  {
//    name1[i] = 0;
//  }
//  name1[18] = '3';
//  name1[19] = '4';
//  
//  char *date1 = malloc(sizeof(char)*20);
//  date1[0] = '1';
//  date1[1] = '0';
//  for (int i = 2; i < 10; i++)
//  {
//    date1[i] = 0;
//  }
//  date1[10] = '5';
//  date1[11] = '6';
//  
//  StructCommand* command = CreateCommand(name1);
//  command->info.count_actions = 1;
//  command->info.num_act_rep = 1;
//  command->info.iterable_actions = 1;
//  command->info.combined = 1;
//  command->info.date = date1;
//  for (int i = 0; i < command->info.count_actions; i++)
//  {
//    StructAction *action = CreateAction();
//    command->addr_actions[i] = (uint32_t)action;
//  }
//  StructAction *action = (StructAction*)command->addr_actions[0];
//  
//  action->thumb_finger = 1;
//  action->pointer_finger = 2;
//  action->middle_finger = 3;
//  action->ring_finder = 4;
//  action->little_finger = 5;
//  action->state_pos_brush = 1;
//  action->state_pos_thumb = 0;
//  action->del_action = 60;
//  
//  AppendCommand(listCommands,command);
//  
//  char *name2 = malloc(sizeof(char)*20);
//  name2[0] = '1';
//  name2[1] = '4';
//  
//  char *date2 = malloc(sizeof(char)*20);
//  date2[0] = '0';
//  date2[1] = '1';
//  for (int i = 2; i < 12; i++)
//  {
//    date2[i] = 0;
//  }
//  
//  for (int i = 2; i < 20; i++)
//  {
//    name2[i] = 0;
//  }
//  command = CreateCommand(name2);
//  command->info.date = date2;
//  command->info.count_actions = 2;
//  command->info.num_act_rep = 1;
//  command->info.iterable_actions = 1;
//  command->info.combined = 1;
//  for (int i = 0; i < command->info.count_actions; i++)
//  {
//    StructAction *action = CreateAction();
//    command->addr_actions[i] = (uint32_t)action;
//  }
//  
//  action = (StructAction*)command->addr_actions[0];
//  
//  action->thumb_finger = 5;
//  action->pointer_finger = 4;
//  action->middle_finger = 3;
//  action->ring_finder = 2;
//  action->little_finger = 1;
//  action->state_pos_brush = 0;
//  action->state_pos_thumb = 0;
//  action->del_action = 120;
//  
//  action = (StructAction*)command->addr_actions[1];
//  
//  action->thumb_finger = 255;
//  action->pointer_finger = 254;
//  action->middle_finger = 253;
//  action->ring_finder = 252;
//  action->little_finger = 251;
//  action->state_pos_brush = 1;
//  action->state_pos_thumb = 1;
//  action->del_action = 20;
//  
//  AppendCommand(listCommands,command);
//  SaveAllCommands(listCommands);


//==============================================================================
//============================File System=======================================
//==============================================================================
// #define StartPage 0x0801FC00
#define StartPage 0x0801EC00
#define SizePage 5

// API
void SaveAllCommands(ListCommands* listCommands);
StructCommand* LoadCommand(char* nameCommand);
int ShowCountCommands();
char* GetNameById(int id);

// Private 
void ClearCommandFlash();
void LoadString(char* name,char* address, uint32_t size);

#endif