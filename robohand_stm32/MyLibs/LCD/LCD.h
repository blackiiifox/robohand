#ifndef LCD_I2C
#define LCD_I2C

#include "stm32f1xx_hal.h"
// ����� PCF8574
#define LCD_ADDR	0x27


// �� ����� �������������.
// �������� ����� ����������� ������
#define PCF_P0	0
#define PCF_P1	1
#define PCF_P2	2
#define PCF_P3	3
#define PCF_P4	4
#define PCF_P5	5
#define PCF_P6	6
#define PCF_P7	7

// ������������ ����� ��� � ����������� ������. �������� ��� ���������������� ��� ����
#define DB4		PCF_P4
#define DB5		PCF_P5
#define DB6		PCF_P6
#define DB7		PCF_P7
#define EN		PCF_P2
#define RW		PCF_P1
#define RS		PCF_P0
#define BL		PCF_P3



//API
void LCD_Init();//������������ LCD
void LCD_write(unsigned char* string);//����� ������ �� ����� LCD
void LCD_set_cursor(uint8_t data);//����� ������� � ������ ���������(�� �������)
void lcd_Backlight(uint8_t state); // ���������/���������� ���������
void I2C_Scan(UART_HandleTypeDef* huart);



// ������� ����������
void lcd_Send(uint8_t data); // ��������� �������� � ���
void lcd_Command(uint8_t com); // ��������� �������� � ���
void lcd_Data(uint8_t com);  // ��������� ������ � ���


void LCD_I2C_Send(uint8_t data);//�������� �� ����� I2C �� �������
void LCD_port_init();//������������ ������ LCD
void send_LCD(unsigned char d_RS, uint8_t data);//�������� ������ �� ����
void Delay(unsigned int Val); //�������� ��� ��������
unsigned int searh_russ_code(unsigned int simbol);//����� ������� �������� � ������� �������
void send_data(uint8_t data);//�������� ������ �� �������
#endif