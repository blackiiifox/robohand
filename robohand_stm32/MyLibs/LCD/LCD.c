#include "LCD.h"
#include <stdlib.h>
#include <string.h>

//������ ��� �������� � ������� �����
unsigned char mass_Russian_simbol[2][66] = {
  '�', '�', '�', '�', '�', '�', '�', '�', '�',
  '�', '�', '�', '�', '�', '�', '�', '�', '�',
  '�', '�', '�', '�', '�', '�', '�', '�', '�',
  '�', '�', '�', '�', '�', '�', '�', '�', '�',
  '�', '�', '�', '�', '�', '�', '�', '�', '�',
  '�', '�', '�', '�', '�', '�', '�', '�', '�',
  '�', '�', '�', '�', '�', '�', '�', '�', '�',
  '�', '�', '�',
  0x41, 0xA0, 0x42, 0xA1, 0xE0, 0x45, 0xA2, 0xA3, 0xA4, 
  0xA5, 0xA6, 0x4B, 0xA7, 0x4D, 0x48, 0x4F, 0xA8, 0x50,
  0x43, 0x54, 0xA9, 0xAA, 0x58, 0xE1, 0xAB, 0xAC, 0xE2, 
  0xAD, 0xAE, 0x62, 0xAF, 0xB0, 0xB1, 0x61, 0xB2, 0xB3, 
  0xB4, 0xE3, 0x65, 0xB5, 0xB6, 0xB7, 0xB8, 0xA6, 0xBA, 
  0xBB, 0xBC, 0xBD, 0x6F, 0xBE, 0x70, 0x63, 0xBF, 0x79, 
  0xE4, 0x78, 0xE5, 0xC0, 0xC1, 0xE6, 0xC2, 0xC3, 0xC4,
  0xC5, 0xC6, 0xC7
};



/*==================================================*/
/*=============������������ ������ �������==========*/
/*==================================================*/
void LCD_port_init()
{
  //    RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
  //    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
  // 
  //    /* Configure I2C_EE pins: SCL and SDA */
  //    //Pin_6 - SCL,Pin7-SDA
  //    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6 | GPIO_Pin_7;
  //    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  //    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
  //    GPIO_Init(GPIOB, &GPIO_InitStructure);
  // 
  //    /* I2C configuration */
  //    I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
  //    I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
  //    I2C_InitStructure.I2C_OwnAddress1 = 0x38;
  //    I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
  //    I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
  //    I2C_InitStructure.I2C_ClockSpeed = 100000;
  // 
  //    /* I2C Peripheral Enable */
  //    I2C_Cmd(I2C1, ENABLE);
  //    /* Apply I2C configuration after enabling it */
  //    I2C_Init(I2C1, &I2C_InitStructure);
}
/*===================================================*/



/*==================================================*/
/*====================������ � LCD==================*/
/*==================================================*/

uint8_t backlightState = 1;
I2C_HandleTypeDef* hi2c = NULL;

void LCD_Init(I2C_HandleTypeDef* hi2cInit){
  hi2c = hi2cInit;
  // LCD_port_init();
  Delay(30000);
  lcd_Command(0x33);
  Delay(300);
  lcd_Command(0x33);
  lcd_Command(0x28);
  lcd_Command(0x08);
  lcd_Command(0x01);
  Delay(300);
  lcd_Command(0x06);
  lcd_Command(0x0C);
}

void I2C_Scan(UART_HandleTypeDef* huart) {
  char info[] = "Scanning I2C bus...\r\n";
  HAL_UART_Transmit(huart, (uint8_t*)info, strlen(info),
                    HAL_MAX_DELAY);
  
  HAL_StatusTypeDef res;
  for(uint16_t i = 0; i < 128; i++) {
    res = HAL_I2C_IsDeviceReady(hi2c, i << 1, 1, 10);
    if(res == HAL_OK) {
      char msg[64];
      snprintf(msg, sizeof(msg), "0x%02X", i); 
      HAL_UART_Transmit(huart, (uint8_t*)msg, strlen(msg),
                        HAL_MAX_DELAY);
    } else {
      HAL_UART_Transmit(huart, (uint8_t*)".", 1,
                        HAL_MAX_DELAY);
    }
  }   
  
  HAL_UART_Transmit(huart, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);
}

void lcd_Command(uint8_t com) {
  uint8_t data = 0;
  
  data |= (backlightState & 0x01) << BL;
  
  data |= (((com & 0x10) >> 4) << DB4);
  data |= (((com & 0x20) >> 5) << DB5);
  data |= (((com & 0x40) >> 6) << DB6);
  data |= (((com & 0x80) >> 7) << DB7);
  lcd_Send(data);
  
  data |= (1 << EN);
  lcd_Send(data);
  HAL_Delay(5);
  
  data &= ~(1 << EN);
  lcd_Send(data);
  HAL_Delay(5);
  
  data = 0;
  
  data |= (backlightState & 0x01) << BL;
  
  data |= (((com & 0x01) >> 0) << DB4);
  data |= (((com & 0x02) >> 1) << DB5);
  data |= (((com & 0x04) >> 2) << DB6);
  data |= (((com & 0x08) >> 3) << DB7);
  lcd_Send(data);
  
  data |= (1 << EN);
  lcd_Send(data);
  HAL_Delay(5);
  
  data &= ~(1 << EN);
  lcd_Send(data);
  HAL_Delay(20);
}


void lcd_Send(uint8_t data) {
  HAL_I2C_Master_Transmit(hi2c, (LCD_ADDR << 1),&data, 1, HAL_MAX_DELAY); 
  //	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY));
  //	I2C_GenerateSTART(I2C1, ENABLE);
  //
  //	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));
  //	I2C_Send7bitAddress(I2C1, ((LCD_ADDR) << 1), I2C_Direction_Transmitter);
  //        Delay(1500);
  //	//while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
  //
  //	I2C_SendData(I2C1, data);
  //	//while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  //        Delay(1500);
  //	I2C_GenerateSTOP(I2C1, ENABLE);
}



void LCD_I2C_Send(uint8_t data)
{
  // HAL_I2C_Master_Transmit();
  
  //  while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY));
  //  I2C_GenerateSTART(I2C1, ENABLE);
  //  
  //  while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));
  //  I2C_Send7bitAddress(I2C1, ((0x20+LCD_ADDR) << 1), I2C_Direction_Transmitter);
  //  
  //  I2C_SendData(I2C1, data);
  //  I2C_GenerateSTOP(I2C1, ENABLE);
}
//������� ������������ ������� � ��������� ������


//�������� ������ �� �������(������ �������)
void send_LCD(unsigned char d_RS, uint8_t data) {
  uint8_t dataTx = 0;
  
  dataTx |= (backlightState & 0x01) << BL;
  
  dataTx |= (((data & 0x10) >> 4) << DB4);
  dataTx |= (((data & 0x20) >> 5) << DB5);
  dataTx |= (((data & 0x40) >> 6) << DB6);
  dataTx |= (((data & 0x80) >> 7) << DB7);
  lcd_Send(dataTx);
  
  dataTx |= (d_RS << RS);
  dataTx |= (1 << EN);
  lcd_Send(dataTx);
  HAL_Delay(5);
  
  dataTx &= ~(1 << EN);
  lcd_Send(dataTx);
  HAL_Delay(5);
  
  dataTx = 0;
  
  dataTx |= (backlightState & 0x01) << BL;
  
  dataTx |= (((data & 0x01) >> 0) << DB4);
  dataTx |= (((data & 0x02) >> 1) << DB5);
  dataTx |= (((data & 0x04) >> 2) << DB6);
  dataTx |= (((data & 0x08) >> 3) << DB7);
  lcd_Send(dataTx);
  
  dataTx |= (d_RS << RS);
  dataTx |= (1 << EN);
  lcd_Send(dataTx);
  HAL_Delay(5);
  
  dataTx &= ~(1 << EN);
  lcd_Send(dataTx);
  HAL_Delay(5);

  HAL_Delay(20); //13 ��.���
}


//������� ������� �������� � ��� LCD
unsigned int searh_russ_code(unsigned int simbol)
{
  for (unsigned int i = 0; i < 66; i++)
  {
    if (mass_Russian_simbol[0][i] == simbol)
      return mass_Russian_simbol[1][i];
  }
  return 0xFF;
}

//�������� ������ �� �������(������� �������)
void send_data(uint8_t data)
{
  send_LCD(1, data);
}


//����� ������ �� �������(������� �������)
void LCD_write(unsigned char* string)
{
  int i = 0;
  while (string[i] != '\0')
  {
    uint8_t code = 0;
    code = searh_russ_code(string[i]);
    if(code == 0xFF) send_data(string[i]);
    else send_data(code);
    i++;
  }
}

void Clear_LCD()
{
  send_LCD(0, 0x01); //������� ������� �������
  Delay(120000);
}

//����� ������� � ������ ���������
void LCD_set_cursor(uint8_t data)
{
  send_LCD(0, data | (1 << 7));
}

//��������
void Delay( unsigned int micros) {
  micros = micros*36*2;//���������� ������
  for( ; micros != 0; micros--) {
    __NOP(); //1 ���� ���������� ����� 0.13 ����
  }
}