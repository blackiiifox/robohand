/* USER CODE BEGIN Header */
/**
******************************************************************************
* @file           : main.c
* @brief          : Main program body
******************************************************************************
** This notice applies to any and all portions of this file
* that are not between comment pairs USER CODE BEGIN and
* USER CODE END. Other portions of this file, whether 
* inserted by the user or by software development tools
* are owned by their respective copyright owners.
*
* COPYRIGHT(c) 2019 STMicroelectronics
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of STMicroelectronics nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h> 
#include "data_handler.h"
#include "file_system.h"
#include "commands.h"
#include "LCD.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* ===========================
Little Finger - Tim2 Chanel1 
RingFinger Finger - Tim2 Chanel2 
Midle Finger - Tim2 Chanel3
Pointer Finger - Tim4 Chanel1
Thumb Finger - Tim4 Chanel2

Brush - Tim4 Chanel3
Thumb - Tim4 Chanel4
=========================== */



char* ModeMIO = "ModeMIO";
char* ModeVoice = "ModeVoice";
char* ModeMixed = "ModeMixed";

typedef struct{
  char OperationMode[20];       // string mode
  char mioTest;                 // 0 - off, 1 - on
} StructProthesisSettings;
StructProthesisSettings prothesisSettings;

// For receive
uint8_t dataRx;
StructPackageBuffer receivePackage;
StructPackageBuffer transmitPackage;

// Current command
typedef struct{
  unsigned long time_delay_actions;
  uint32_t indexAction;
  uint32_t currentRepeat;
  StructCommand* current_command;
} StructInfoCurrentCommand;

StructInfoCurrentCommand currentCommand;

// Time system
uint32_t time_last_receive = 0;

// Mio sensor
volatile uint16_t ADC_Data[2];

//Servos veriables
#define LittleFinger    0
#define RingFinger      1
#define MiddleFinger    2
#define PointerFinger   3
#define ThumbFinger     4
#define Brush           5

// Timers veriables
#define LittleFingerTim TIM4->CCR2
#define RingFingerTim TIM4->CCR3
#define MiddleFingerTim TIM2->CCR2
#define PointerFingerTim TIM2->CCR3
#define ThumbFingerTim TIM2->CCR1
#define BrushTim TIM4->CCR4

//Servos settings
//Servos max          Little Ring    Middle  Pointer    Thumb     Brush    
int Max_position[] = {2300,  2300,   600,   2200,      600,   2330};
int Min_position[] = {500,   500,    2300,    600,       2200,    630};
bool state_servo_set = false;
unsigned long last_servo_set = 0;

// Finger structur
typedef struct{
  uint32_t thumb_finger_angle;
  uint32_t pointer_finger_angle;
  uint32_t middle_finger_angle;
  uint32_t ring_finder_angle;
  uint32_t little_finger_angle;
  uint32_t brush_angle;
  uint32_t thumb_angle;
} SturctServosAngle;


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

// System commands
#define CommandSave 0x15
#define CommandVoiceExex 0x16
#define CommandExex 0x17
#define CommandExexRaw 0x18
#define CommandSaveToVoice 0x19
#define CommandActionListRequest 0x20
#define CommandActionListAnswer 0x21

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

// =============================================================================
// ==============================FUNCTIONS======================================
// =============================================================================
int Palm_corner(int Palm, int Corner);                          // Convert angle to Tim value
void RecognizePackage(StructPackage* package);                  // recognize and convert receive package
void ConvertPackageToListCommands(StructPackage* package);      // Convert receive package to List hand commands1
StructCommand* ConvertArrayToCommand(uint32_t* array);          // Convert receive package to command
void ClearCurrentCommand();                                     // Clear command if it is done
char* ConvertPackageToNameCommand(StructPackage* package);      // Convert receive package to char array with name command
void setCurrentCommand(StructCommand* new_current_command);     // Set new current command in system
void ClearProthesisSettings();                                  // Set base prothesis settinings
void SetProthesisMode(char* newMode);                           // Set new mode prothesis
char ExecuteBasicCommand(char* nameCommand);                    // Execute receive command if its basic command (not action)
void ServosSet(SturctServosAngle* newAngle);                    // Set new angle in servos
void ServosStop();                                              // Stop servo
void ServoSetRaw(SturctServosAngle* newAngle);                  // Set raw data to servo
void TrasnmitPackage(StructPackage* package);                   // Transmit package to serial
void InitServoTimers();                                         // Base timers initilization, all positions in 0
void DisableServos();                                           // Disables servos to prevent overloading.

void InitServoTimers()
{
  TIM2->CCR1 = 0;
  TIM2->CCR2 = 0;
  TIM2->CCR3 = 0;
  TIM4->CCR2 = 0;
  TIM4->CCR3 = 0;
  TIM4->CCR4 = 0;
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
  HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2);
  HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_3);
  HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_4);
}

void ServoSetRaw(SturctServosAngle* newAngle)
{
  // Need macros for set in servo
  // Set finger
  LittleFingerTim = newAngle->little_finger_angle;
  RingFingerTim = newAngle->ring_finder_angle;
  MiddleFingerTim = newAngle->middle_finger_angle;
  PointerFingerTim = newAngle->pointer_finger_angle;
  ThumbFingerTim = newAngle->thumb_finger_angle;
  // Set brush
  BrushTim = newAngle->brush_angle;
  
  state_servo_set = true;
  last_servo_set = HAL_GetTick();
}

void ServosSet(SturctServosAngle* newAngle)
{
  SturctServosAngle raw_angles;
  
  raw_angles.little_finger_angle = Palm_corner(LittleFinger, newAngle->little_finger_angle);
  // Set finger
  raw_angles.little_finger_angle = Palm_corner(LittleFinger, newAngle->little_finger_angle);
  raw_angles.ring_finder_angle = Palm_corner(RingFinger, newAngle->ring_finder_angle);
  raw_angles.middle_finger_angle = Palm_corner(MiddleFinger, newAngle->middle_finger_angle);
  raw_angles.pointer_finger_angle = Palm_corner(PointerFinger, newAngle->pointer_finger_angle);
  raw_angles.thumb_finger_angle = Palm_corner(ThumbFinger, newAngle->thumb_finger_angle);
  // Set brush
  raw_angles.brush_angle = Palm_corner(Brush, newAngle->brush_angle);
  
  ServoSetRaw(&raw_angles);
}

void DisableServos()
{
  SturctServosAngle raw_angles;
  raw_angles.brush_angle = 0;
  raw_angles.little_finger_angle = 0;
  raw_angles.middle_finger_angle = 0;
  raw_angles.pointer_finger_angle = 0;
  raw_angles.ring_finder_angle = 0;
  raw_angles.thumb_finger_angle = 0;
  ServoSetRaw(&raw_angles);
}

void ServosStop()
{
  HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_1);
  HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_2);
  HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_3);
  HAL_TIM_PWM_Stop(&htim4, TIM_CHANNEL_2);
  HAL_TIM_PWM_Stop(&htim4, TIM_CHANNEL_3);
  HAL_TIM_PWM_Stop(&htim4, TIM_CHANNEL_4);
}

void TrasnmitPackage(StructPackage* package)
{
  HAL_UART_Transmit(&huart1, package->package, package->countData, 0xFFFF);
}

void ClearProthesisSettings()
{
  prothesisSettings.mioTest = 0;
  for(int i = 0; i < 20; i++)
  {
    prothesisSettings.OperationMode[i] = 0;
  }
}

void SetProthesisMode(char* newMode)
{
  ClearProthesisSettings();
  for(int i = 0; i < 20; i++)
  {
    if(newMode[i] == 0)
    {
      break;
    }
    
    prothesisSettings.OperationMode[i] = newMode[i];
  }
}

void setCurrentCommand(StructCommand* new_current_command)
{
  currentCommand.current_command = new_current_command;
  currentCommand.indexAction = 0;
  currentCommand.time_delay_actions = 0;
  currentCommand.currentRepeat = 0;
}

void clearInfoCurrentCommand()
{
  currentCommand.current_command = NULL;
  currentCommand.indexAction = 0;
  currentCommand.time_delay_actions = 0;
  currentCommand.currentRepeat = 0;
}

int Palm_corner(int Palm, int Corner) 
{
  if(Corner < 0)
  {
    Corner = 0;
  }
  
  if(Corner > 180)
  {
    Corner = 180;
  }
  
  int value;
  
  if(Palm == MiddleFinger || Palm == ThumbFinger)
  {
    value = (int)(Max_position[Palm] - (int)((Max_position[Palm] - Min_position[Palm]) / 180 * (180 - Corner)));
  }
  else
  {
    value = (int)(Min_position[Palm] + (int)((Max_position[Palm] - Min_position[Palm]) / 180 * Corner));
  }
  
  return value;
}

void RecognizePackage(StructPackage* recPackage)
{
  switch ( recPackage->package[0] ) {
  case CommandSave:
    {
      ConvertPackageToListCommands(recPackage);
      break;
    }
  case CommandVoiceExex:
    {
      ClearCurrentCommand();
      char* nameCommand = ConvertPackageToNameCommand(recPackage);
      if(!ExecuteBasicCommand(nameCommand) == 1)
      {
        StructCommand* newCommand = LoadCommand(nameCommand);
        if(newCommand != NULL)
        {
          setCurrentCommand(newCommand);
        }
      }
      
      free(nameCommand);
      break;
    }
  case CommandExex:
    {
      ClearCurrentCommand();
      break;
    }
  case CommandExexRaw:
    {
      uint32_t valueAnggle = *((uint32_t*)(&(recPackage->package[1])));
      SturctServosAngle servo_angle;
      servo_angle.brush_angle = valueAnggle;
      servo_angle.little_finger_angle = valueAnggle;
      servo_angle.middle_finger_angle = valueAnggle;
      servo_angle.pointer_finger_angle = valueAnggle;
      servo_angle.ring_finder_angle = valueAnggle;
      servo_angle.thumb_angle = valueAnggle;
      servo_angle.thumb_finger_angle = valueAnggle;
      ServoSetRaw(&servo_angle);
      break;
    }
    
  case CommandActionListRequest:
    {
      // Test
      int count_commands = ShowCountCommands();
      int lenght_data = count_commands * 20 + 1;
      uint8_t* data_package = malloc((sizeof(uint8_t)*lenght_data));;
      data_package[0] = CommandActionListAnswer;
      for (int i = 0; i < count_commands; i++)
      {
        char* name_command = GetNameById(i);
        
        if(name_command == NULL)
        {
          count_commands--;
          lenght_data = lenght_data - 20;
          free(name_command);
          continue;
        }
        
        uint8_t* new_filed = &(data_package[i*20 + 1]);
        
        for(int i = 0; i < 20; i++)
        {
          new_filed[i] = name_command[i];
        }
        
        free(name_command);
      }
      
      StructPackage* currentPackage = Create_Transmit_Package(addressVoice, data_package, lenght_data);
      PackageBufAdd(&transmitPackage, currentPackage);
      free(data_package);
      break; 
    }
    
  }
  
  free(recPackage->package);
  free(recPackage);
}

char ExecuteBasicCommand(char* nameCommand)
{
  char stateBasic = 0;
  
  if(strcmp(nameCommand, ModeVoice) == 0)
  {
    SetProthesisMode(ModeVoice);
    stateBasic = 1;
  }
  
  if(strcmp(nameCommand, ModeMIO) == 0)
  {
    SetProthesisMode(ModeMIO);
    stateBasic = 1;
  } 
  
  if(strcmp(nameCommand, ModeMixed) == 0)
  {
    SetProthesisMode(ModeMixed);
    stateBasic = 1;
  } 
  
  return stateBasic;
}

void ClearCurrentCommand()
{
  if(currentCommand.current_command != NULL)
  {
    DeleteCommand(currentCommand.current_command);
    clearInfoCurrentCommand();
  }
}

char* ConvertPackageToNameCommand(StructPackage* package)
{
  char* name = NULL;
  if(package->countData == 21)
  {
    name = malloc(sizeof(char)*20);
    LoadString(name, (char*)&(package->package[1]), 20);
  }
  
  return name;
}

void ConvertPackageToListCommands(StructPackage* package)
{
  int index = 1;
  
  if(package->countData > 1)
  {
    ListCommands* listCommands = CreateListCommands();
    while (index < package->countData)
    {
      StructCommand* newCommand = ConvertArrayToCommand((uint32_t*)&(package->package[index]));
      AppendCommand(listCommands, newCommand);
      index = index + (newCommand->info.count_actions*8) + 36;
    }
    
    SaveAllCommands(listCommands);
    DeleateListCommands(listCommands);
  }
}

StructCommand* ConvertArrayToCommand(uint32_t* array)
{
  StructCommand* command = NULL;
  uint32_t address_command = (uint32_t)array;
  
  char *nameCommandInSystem = malloc(sizeof(char)*20);
  LoadString(nameCommandInSystem, (char*)address_command, 20);
  
  command = CreateCommand(nameCommandInSystem);
  // info load
  char *dateCommandInSystem = malloc(sizeof(char)*12);
  LoadString(dateCommandInSystem, (char*)(address_command + 0x14), 12);
  command->info.date = dateCommandInSystem;
  
  uint32_t info = *((uint32_t*)(address_command + 0x20));
  command->info.combined = info&0x000000FF;
  command->info.iterable_actions =(info&0x0000FF00) >> 8;
  command->info.num_act_rep = (info&0x00FF0000) >> 16;
  command->info.count_actions = (info&0xFF000000) >> 24;
  
  uint32_t address_action = address_command + 0x24;
  for (int i = 0; i < command->info.count_actions; i++)
  {
    StructAction *action = CreateAction();
    uint32_t action_low = *(uint32_t*)address_action;
    uint32_t action_high = *(uint32_t*)((uint32_t*)address_action + 1);
    
    action->little_finger = action_low&0x000000FF;
    action->ring_finder = (action_low&0x0000FF00) >> 8;
    action->middle_finger = (action_low&0x00FF0000) >> 16;
    action->pointer_finger = (action_low&0xFF000000) >> 24;
    
    action->del_action = action_high&0x000000FF;
    action->state_pos_thumb = (action_high&0x0000FF00) >> 8;
    action->state_pos_brush = (action_high&0x00FF0000) >> 16;
    action->thumb_finger = (action_high&0xFF000000) >> 24;
    address_action = address_action + 8;
    command->addr_actions[i] = (uint32_t)action;
  }
  
  return command;
}


// =============================================================================
// =============================INTERRUPTS======================================
// =============================================================================

// ================TIM interrupt========================
int i = 0;
int stateTrI;
void HAL_TIM_PeriodElapsedCallback (TIM_HandleTypeDef *htim)
{
  if (htim == &htim3)
  {
    HAL_ADC_Start_IT(&hadc1);
  }
}


// ================UART interrupt========================
void HAL_UART_TxCpltCallback (UART_HandleTypeDef *huart)
{
  if (huart == &huart1)
  {
  }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  if (huart == &huart1)
  {
    uint32_t time = HAL_GetTick();
    if((time - time_last_receive) > 1000)
    {
      Clear_Data_Receive();
    }
    
    Append_Data_Receive(dataRx);
    time_last_receive = time;
    HAL_UART_Receive_IT(&huart1, &dataRx, 1);
  }
}

// ================ADC interrupt========================
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{
  // adc_value = HAL_ADC_GetValue(&hadc1);
  HAL_ADC_Stop_IT (&hadc1);
  int sensor1 = ADC_Data[0];
  int sensor2 = ADC_Data[1];
  
  char string_sensor1[6];
  sprintf(string_sensor1, "%d", sensor1);
  char string_sensor2[6];
  sprintf(string_sensor2, "%d", sensor2);
  
  uint8_t result_t[10];
  
  int index_stop = 0;
  for (index_stop = 0; index_stop < 4; index_stop++)
  {
    if(string_sensor1[index_stop] == '\0')
    {
      break;
    }
    
    result_t[index_stop] = string_sensor1[index_stop];
  }
  
  result_t[index_stop] = ' ';
  index_stop++;
  for (int i = 0; i < 4; i++)
  {
    if(string_sensor2[i] == '\0')
      break;
    result_t[index_stop] = string_sensor2[i];
    index_stop++;
  }
  
  result_t[index_stop] = '\n';
  HAL_UART_Transmit_IT(&huart1, result_t, index_stop+1);
}

unsigned char countData = 0;
/* USER CODE END 0 */

/**
* @brief  The application entry point.
* @retval int
*/
int main(void)
{
  /* USER CODE BEGIN 1 */
  
  /* USER CODE END 1 */
  
  /* MCU Configuration--------------------------------------------------------*/
  
  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();
  
  /* USER CODE BEGIN Init */
  
  /* USER CODE END Init */
  
  /* Configure the system clock */
  SystemClock_Config();
  
  /* USER CODE BEGIN SysInit */
  
  /* USER CODE END SysInit */
  
  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_TIM2_Init();
  MX_TIM4_Init();
  MX_TIM3_Init();
  MX_ADC1_Init();
  MX_I2C2_Init();
  /* USER CODE BEGIN 2 */
  Create_Data_Receive();
  CreatePackageBuffer(&receivePackage);
  CreatePackageBuffer(&transmitPackage);
  SetPackageBuf(&receivePackage);
  clearInfoCurrentCommand();
  ClearProthesisSettings();
  SetProthesisMode(ModeVoice);
  
  // HAL_ADC_Start_DMA(&hadc1,(uint32_t*) &ADC_Data,2);
  HAL_UART_Receive_IT(&huart1, &dataRx, 1);
  InitServoTimers(); 
  
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
  
  // HAL_TIM_Base_Start_IT(&htim3); // MIO start
  
  /* USER CODE END 2 */
  
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  SturctServosAngle servo_angle;
  
  //  ThumbFingerTim = 500;
  //  ThumbFingerTim = 1500;
  //  ThumbFingerTim = 2500;
  //  ThumbFingerTim = 1500;
  //  ThumbFingerTim = 500;
  //  while(1);
  bool state_blue_led = false;
  long last_blue_set = 0;
  
  while (1)
  {    
    
    if(state_servo_set == true)
    {
      if(HAL_GetTick() - last_servo_set > 2500)
      {
        DisableServos();
        state_servo_set = false;
      }      
    }
    
    if(state_blue_led == true)
    {
      if(HAL_GetTick() - last_blue_set > 500)
      {
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
        last_blue_set = 0;
        state_blue_led = false;
      }      
    }
    
    if(PackageBufferCount(&receivePackage) > 0)
    {
      HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
      state_blue_led = true;
      last_blue_set = HAL_GetTick();
      StructPackage* currentPackage = PackageBufGet(&receivePackage);
      RecognizePackage(currentPackage);
    }
    
    if(PackageBufferCount(&transmitPackage) > 0)
    {
      HAL_Delay(500);
      StructPackage* currentPackage = PackageBufGet(&transmitPackage);
      TrasnmitPackage(currentPackage);
    }
    
    if (strcmp(prothesisSettings.OperationMode, ModeMIO) != 0)
    {
      if(currentCommand.current_command != NULL)
      {
        if (strcmp(prothesisSettings.OperationMode, ModeMixed) == 0 && currentCommand.current_command->info.combined == 0)
        {
          ClearCurrentCommand();
          continue;
        }
        // if the current time from the start is greater than or equal to the time
        // when system need to perform actions, then perform the action
        unsigned long millis = HAL_GetTick();
        if(currentCommand.time_delay_actions<=millis)
        {
          StructAction* action = (StructAction*)currentCommand.current_command->addr_actions[currentCommand.indexAction];
          // set new action in timers
          // set finger
          servo_angle.little_finger_angle = action->little_finger;
          servo_angle.middle_finger_angle = action->middle_finger;
          servo_angle.pointer_finger_angle = action->pointer_finger;
          servo_angle.ring_finder_angle = action->ring_finder;
          servo_angle.thumb_finger_angle = action->thumb_finger;
          // set thumb and brush
          servo_angle.brush_angle = action->state_pos_brush;
          servo_angle.thumb_angle = action->state_pos_thumb;
          ServosSet(&servo_angle);
          
          // check end command
          currentCommand.time_delay_actions = action->del_action*100 + millis;
          currentCommand.indexAction++;
          
          if(currentCommand.current_command->info.count_actions == currentCommand.indexAction)
          {
            if(currentCommand.current_command->info.iterable_actions!=0)
            {
              currentCommand.indexAction = 0;
            }
            else
            {
              if(currentCommand.current_command->info.num_act_rep - 1 > currentCommand.currentRepeat)
              {
                currentCommand.indexAction = 0;
                currentCommand.currentRepeat++;
              }
              else 
              {
                ClearCurrentCommand();
              }
            } 
          }
        }
      }
    }
    else
    {
      if(ADC_Data[0] > 300)
      {
        servo_angle.little_finger_angle = 255;
        servo_angle.middle_finger_angle = 255;
        servo_angle.pointer_finger_angle = 255;
        servo_angle.ring_finder_angle = 255;
        servo_angle.thumb_finger_angle = 255;
        ServosSet(&servo_angle);
      }
      else
      {
        servo_angle.little_finger_angle = 0;
        servo_angle.middle_finger_angle = 0;
        servo_angle.pointer_finger_angle = 0;
        servo_angle.ring_finder_angle = 0;
        servo_angle.thumb_finger_angle = 0;
        ServosSet(&servo_angle);
      }
    }
    
    /* USER CODE END WHILE */
    
    /* USER CODE BEGIN 3 */
    
  }
  /* USER CODE END 3 */
}

/**
* @brief System Clock Configuration
* @retval None
*/
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};
  
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
    |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
* @brief  This function is executed in case of error occurrence.
* @retval None
*/
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
* @brief  Reports the name of the source file and the source line number
*         where the assert_param error has occurred.
* @param  file: pointer to the source file name
* @param  line: assert_param error line source number
* @retval None
*/
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
  tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
