EESchema Schematic File Version 4
LIBS:RoboHand-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_ST_STM32F1:STM32F103C8Tx U1
U 1 1 5BB99066
P 6000 3600
F 0 "U1" H 5950 2014 50  0000 L CNN
F 1 "STM32F103C8Tx" H 5950 1923 50  0000 L CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 5400 2200 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00161566.pdf" H 6000 3600 50  0001 C CNN
	1    6000 3600
	1    0    0    -1  
$EndComp
$Comp
L Motor:Motor_Servo M3
U 1 1 5BB991C0
P 3600 3500
F 0 "M3" H 3931 3565 50  0000 L CNN
F 1 "Motor_Servo" H 3931 3474 50  0000 L CNN
F 2 "" H 3600 3310 50  0001 C CNN
F 3 "http://forums.parallax.com/uploads/attachments/46831/74481.png" H 3600 3310 50  0001 C CNN
	1    3600 3500
	1    0    0    -1  
$EndComp
$Comp
L Motor:Motor_Servo M4
U 1 1 5BB99295
P 3600 4000
F 0 "M4" H 3931 4065 50  0000 L CNN
F 1 "Motor_Servo" H 3931 3974 50  0000 L CNN
F 2 "" H 3600 3810 50  0001 C CNN
F 3 "http://forums.parallax.com/uploads/attachments/46831/74481.png" H 3600 3810 50  0001 C CNN
	1    3600 4000
	1    0    0    -1  
$EndComp
$Comp
L Motor:Motor_Servo M5
U 1 1 5BB992CD
P 3600 4500
F 0 "M5" H 3931 4565 50  0000 L CNN
F 1 "Motor_Servo" H 3931 4474 50  0000 L CNN
F 2 "" H 3600 4310 50  0001 C CNN
F 3 "http://forums.parallax.com/uploads/attachments/46831/74481.png" H 3600 4310 50  0001 C CNN
	1    3600 4500
	1    0    0    -1  
$EndComp
$Comp
L Motor:Motor_Servo M1
U 1 1 5BB99316
P 3600 2500
F 0 "M1" H 3931 2565 50  0000 L CNN
F 1 "Motor_Servo" H 3931 2474 50  0000 L CNN
F 2 "" H 3600 2310 50  0001 C CNN
F 3 "http://forums.parallax.com/uploads/attachments/46831/74481.png" H 3600 2310 50  0001 C CNN
	1    3600 2500
	1    0    0    -1  
$EndComp
Entry Wire Line
	2800 2500 2900 2400
Entry Wire Line
	4600 4100 4700 4000
$Comp
L Motor:Motor_Servo M2
U 1 1 5BB9910E
P 3600 3000
F 0 "M2" H 3931 3065 50  0000 L CNN
F 1 "Motor_Servo" H 3931 2974 50  0000 L CNN
F 2 "" H 3600 2810 50  0001 C CNN
F 3 "http://forums.parallax.com/uploads/attachments/46831/74481.png" H 3600 2810 50  0001 C CNN
	1    3600 3000
	1    0    0    -1  
$EndComp
Entry Wire Line
	2800 3000 2900 2900
Entry Wire Line
	2800 3500 2900 3400
Entry Wire Line
	2800 4000 2900 3900
Entry Wire Line
	4600 4200 4700 4100
Entry Wire Line
	4600 4300 4700 4200
Entry Wire Line
	4600 4400 4700 4300
Entry Wire Line
	2800 4500 2900 4400
Entry Wire Line
	4600 5700 4700 5600
$Comp
L Motor:Motor_Servo M6
U 1 1 5BB93B21
P 3600 5000
F 0 "M6" H 3931 5065 50  0000 L CNN
F 1 "Motor_Servo" H 3931 4974 50  0000 L CNN
F 2 "" H 3600 4810 50  0001 C CNN
F 3 "http://forums.parallax.com/uploads/attachments/46831/74481.png" H 3600 4810 50  0001 C CNN
	1    3600 5000
	1    0    0    -1  
$EndComp
Entry Wire Line
	2800 5000 2900 4900
Entry Wire Line
	4600 5550 4700 5450
Wire Bus Line
	2800 5750 4600 5750
Wire Wire Line
	3300 2400 2900 2400
Wire Wire Line
	2900 2900 3300 2900
Wire Wire Line
	3300 3400 2900 3400
Wire Wire Line
	2900 3900 3300 3900
Wire Wire Line
	2900 4400 3300 4400
Wire Wire Line
	7050 5600 7050 3400
Wire Wire Line
	2900 4900 3300 4900
Wire Wire Line
	6950 5450 6950 3500
Text Label 2950 2400 0    50   ~ 0
PWM_M1
Text Label 2950 2900 0    50   ~ 0
PWM_M2
Text Label 2950 3400 0    50   ~ 0
PWM_M3
Text Label 2950 3900 0    50   ~ 0
PWM_M4
Text Label 2950 4400 0    50   ~ 0
PWM_M5
Text Label 2950 4900 0    50   ~ 0
PWM_M6
Text Label 4750 4000 0    50   ~ 0
PWM_M1
Wire Wire Line
	4700 4000 5300 4000
Wire Wire Line
	4700 4100 5300 4100
Wire Wire Line
	4700 4200 5300 4200
Wire Wire Line
	4700 4300 5300 4300
Text Label 4750 4100 0    50   ~ 0
PWM_M2
Text Label 4750 4200 0    50   ~ 0
PWM_M3
Text Label 4750 4300 0    50   ~ 0
PWM_M4
Text Label 6650 3400 0    50   ~ 0
PWM_M5
Text Label 6650 3500 0    50   ~ 0
PWM_M6
Wire Wire Line
	6600 3400 7050 3400
Wire Wire Line
	4700 5450 6950 5450
Wire Wire Line
	4700 5600 7050 5600
Wire Wire Line
	7550 4300 6600 4300
Wire Wire Line
	7750 4400 6600 4400
Text GLabel 8950 3600 2    50   Input ~ 0
MIO_OUTPUT_1
$Comp
L Device:R R1
U 1 1 5BB9A22A
P 8650 3600
F 0 "R1" V 8443 3600 50  0000 C CNN
F 1 "1KOm" V 8534 3600 50  0000 C CNN
F 2 "" V 8580 3600 50  0001 C CNN
F 3 "~" H 8650 3600 50  0001 C CNN
	1    8650 3600
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5BB9A306
P 8350 4050
F 0 "R2" V 8143 4050 50  0000 C CNN
F 1 "2KOM" V 8234 4050 50  0000 C CNN
F 2 "" V 8280 4050 50  0001 C CNN
F 3 "~" H 8350 4050 50  0001 C CNN
	1    8350 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 3600 8950 3600
$Comp
L power:GND #PWR?
U 1 1 5BB9BFF9
P 8350 4350
F 0 "#PWR?" H 8350 4100 50  0001 C CNN
F 1 "GND" H 8355 4177 50  0000 C CNN
F 2 "" H 8350 4350 50  0001 C CNN
F 3 "" H 8350 4350 50  0001 C CNN
	1    8350 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 3500 6950 3500
Wire Wire Line
	8350 3600 8500 3600
Wire Wire Line
	8350 4200 8350 4350
Text GLabel 8950 4750 2    50   Input ~ 0
MIO_OUTPUT_2
$Comp
L Device:R R3
U 1 1 5BDC0ABB
P 8650 4750
F 0 "R3" V 8443 4750 50  0000 C CNN
F 1 "1KOm" V 8534 4750 50  0000 C CNN
F 2 "" V 8580 4750 50  0001 C CNN
F 3 "~" H 8650 4750 50  0001 C CNN
	1    8650 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	8350 4750 8500 4750
$Comp
L Device:R R4
U 1 1 5BDC0EE3
P 8350 5200
F 0 "R4" V 8143 5200 50  0000 C CNN
F 1 "2000KOm" V 8234 5200 50  0000 C CNN
F 2 "" V 8280 5200 50  0001 C CNN
F 3 "~" H 8350 5200 50  0001 C CNN
	1    8350 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 4750 8350 5050
Wire Wire Line
	8350 5350 8350 5500
Wire Wire Line
	8350 3600 8350 3900
Wire Wire Line
	8800 4750 8950 4750
$Comp
L power:GND #PWR?
U 1 1 5BDC4960
P 8350 5500
F 0 "#PWR?" H 8350 5250 50  0001 C CNN
F 1 "GND" H 8355 5327 50  0000 C CNN
F 2 "" H 8350 5500 50  0001 C CNN
F 3 "" H 8350 5500 50  0001 C CNN
	1    8350 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 4750 7950 4750
Wire Wire Line
	7950 3800 6600 3800
Connection ~ 8350 4750
Entry Wire Line
	4600 5400 4700 5300
Wire Wire Line
	4700 5300 6850 5300
Wire Wire Line
	6850 5300 6850 3600
$Comp
L Motor:Motor_Servo M?
U 1 1 5BDC2527
P 3600 5450
F 0 "M?" H 3931 5515 50  0000 L CNN
F 1 "Motor_Servo" H 3931 5424 50  0000 L CNN
F 2 "" H 3600 5260 50  0001 C CNN
F 3 "http://forums.parallax.com/uploads/attachments/46831/74481.png" H 3600 5260 50  0001 C CNN
	1    3600 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 5350 2900 5350
Text Label 2950 5350 0    50   ~ 0
PWM_M7
Entry Wire Line
	2800 5450 2900 5350
Wire Wire Line
	8150 3600 8350 3600
Connection ~ 8350 3600
Wire Wire Line
	6600 3600 6850 3600
Text Label 6650 3600 0    50   ~ 0
PWM_M7
Wire Wire Line
	8150 3700 8150 3600
Wire Wire Line
	7950 3800 7950 4750
Wire Wire Line
	6600 3700 8150 3700
$Comp
L RF_Bluetooth:RN4871 U2
U 1 1 5BFAAB37
P 8600 2600
F 0 "U2" H 8600 3378 50  0000 C CNN
F 1 "RN4871" H 8600 3287 50  0000 C CNN
F 2 "RF_Module:Microchip_RN4871" H 8600 1900 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/50002489A.pdf" H 8100 3150 50  0001 C CNN
	1    8600 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 2400 7900 2400
Wire Wire Line
	7550 2300 7900 2300
Wire Wire Line
	7550 2300 7550 4300
Wire Wire Line
	7750 2400 7750 4400
$Comp
L power:GND #PWR?
U 1 1 5BFB3842
P 8500 3250
F 0 "#PWR?" H 8500 3000 50  0001 C CNN
F 1 "GND" H 8505 3077 50  0000 C CNN
F 2 "" H 8500 3250 50  0001 C CNN
F 3 "" H 8500 3250 50  0001 C CNN
	1    8500 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 3250 8500 3200
$Comp
L power:+9V #PWR?
U 1 1 5BFBA9D2
P 3200 2200
F 0 "#PWR?" H 3200 2050 50  0001 C CNN
F 1 "+9V" H 3215 2373 50  0000 C CNN
F 2 "" H 3200 2200 50  0001 C CNN
F 3 "" H 3200 2200 50  0001 C CNN
	1    3200 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 2200 3200 2500
Wire Wire Line
	3200 5450 3300 5450
Wire Wire Line
	3200 5000 3300 5000
Connection ~ 3200 5000
Wire Wire Line
	3200 5000 3200 5450
Wire Wire Line
	3300 4500 3200 4500
Connection ~ 3200 4500
Wire Wire Line
	3200 4500 3200 5000
Wire Wire Line
	3300 4000 3200 4000
Connection ~ 3200 4000
Wire Wire Line
	3200 4000 3200 4500
Wire Wire Line
	3300 3500 3200 3500
Connection ~ 3200 3500
Wire Wire Line
	3200 3500 3200 4000
Wire Wire Line
	3300 3000 3200 3000
Connection ~ 3200 3000
Wire Wire Line
	3200 3000 3200 3500
Wire Wire Line
	3300 2500 3200 2500
Connection ~ 3200 2500
Wire Wire Line
	3200 2500 3200 3000
$Comp
L power:GND #PWR?
U 1 1 5BFC1776
P 3000 2200
F 0 "#PWR?" H 3000 1950 50  0001 C CNN
F 1 "GND" H 3005 2027 50  0000 C CNN
F 2 "" H 3000 2200 50  0001 C CNN
F 3 "" H 3000 2200 50  0001 C CNN
	1    3000 2200
	-1   0    0    1   
$EndComp
Wire Wire Line
	3300 2600 3000 2600
Wire Wire Line
	3000 2600 3000 2200
Wire Wire Line
	3300 3100 3000 3100
Connection ~ 3000 2600
Wire Wire Line
	3300 3600 3000 3600
Wire Wire Line
	3000 2600 3000 3100
Connection ~ 3000 3100
Wire Wire Line
	3000 3100 3000 3600
Wire Wire Line
	3000 3600 3000 4100
Wire Wire Line
	3000 5550 3300 5550
Connection ~ 3000 3600
Wire Wire Line
	3300 5100 3000 5100
Connection ~ 3000 5100
Wire Wire Line
	3000 5100 3000 5550
Wire Wire Line
	3300 4600 3000 4600
Connection ~ 3000 4600
Wire Wire Line
	3000 4600 3000 5100
Wire Wire Line
	3300 4100 3000 4100
Connection ~ 3000 4100
Wire Wire Line
	3000 4100 3000 4600
$Comp
L power:+3.3V #PWR?
U 1 1 5BFCE052
P 5750 1900
F 0 "#PWR?" H 5750 1750 50  0001 C CNN
F 1 "+3.3V" H 5765 2073 50  0000 C CNN
F 2 "" H 5750 1900 50  0001 C CNN
F 3 "" H 5750 1900 50  0001 C CNN
	1    5750 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 1900 5750 1950
Wire Wire Line
	5750 1950 6000 1950
Wire Wire Line
	6000 1950 6000 2100
$Comp
L power:GND #PWR?
U 1 1 5BFCFC11
P 5800 5100
F 0 "#PWR?" H 5800 4850 50  0001 C CNN
F 1 "GND" H 5805 4927 50  0000 C CNN
F 2 "" H 5800 5100 50  0001 C CNN
F 3 "" H 5800 5100 50  0001 C CNN
	1    5800 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 1950 8600 1950
Wire Wire Line
	8600 1950 8600 2000
Wire Bus Line
	2800 2250 2800 5750
Wire Bus Line
	4600 4000 4600 5750
Connection ~ 6000 1950
$EndSCHEMATC
